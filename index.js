
const getProfessions = require('./src/professions');
const openCSVStream = require('./tools/csvStreamParser');
const sumData = require('./src/sumData');

const PROFESSIONS_PATH = './data/technical-test-professions.csv';
const JOBS_PATH = './data/technical-test-jobs.csv';

async function main(professionsPath = PROFESSIONS_PATH, jobsPath = JOBS_PATH) {
    const professions = await getProfessions(openCSVStream(professionsPath));

    // Sum the two data
    const data = await sumData(
        openCSVStream(jobsPath), 
        function getDimentionsValues (line) {
        return [line.contract_type, professions.get(line.profession_id) || 'Inconnue'];
        }
    )

    console.table(data);
}
  
main()
  .then(() => console.log('C\'est bon :)'))
  .catch((error) => console.error(error));