module.exports = {
	verbose: true,
	testMatch: ['<rootDir>/**/*.test.js'],
	collectCoverage: true,
	collectCoverageFrom: ['**/*.{js,jsx}', '!**/node_modules/**'],
	moduleFileExtensions: ['js', 'jsx'],
};