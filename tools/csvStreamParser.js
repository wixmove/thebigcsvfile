const parse = require('csv-parse');
const fs = require('fs');

// Get a new CSV stream parser
const getParser = () => parse({ 
  delimiter: ',', 
  columns: true 
});

// Get a stream of parsed CSV lines
module.exports = function openCSVStream(path) {
  return fs.createReadStream(path) // Open a stream from the file
      .pipe(getParser()); // Parses the streamed content with a CSV pattern
}