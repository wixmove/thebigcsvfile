# TheBigCsvFIle
# Pré-requis
Avoir préalablement installé un environnement NodeJS

Suivre le guide d'installation sur le site de  [Node.js](https://nodejs.org/).


# Installation
```sh
$ git clone https://gitlab.com/wixmove/thebigcsvfile.git
$ cd thebigcsvfile/
$ npm install 
```

# Lancement des tests unitaires
```sh
$ npm run test
```

# Lancement du script
```sh
$ npm run convert
```