const getProfessions = require('./professions');

test('Should get an empty list when empty', async () => {
  const professions = getProfessions([]);
  // Check that professions is an instance of Map
  // With no keys/values
});

test('Should get a list', async () => {
  const professions = getProfessions([{category_name: 'bob', id: '1'}]);
  // Check that professions is an instance of Map
  // With one key = '1' and it's value 'bob'
})