module.exports = async function sumData (stream, dimentionsCallback) {
    const resultData = {
      TOTAL: { TOTAL: 0 }
    };
    for await (const line of stream) {
      const [d1, d2] = dimentionsCallback(line);
      // Creating the data structure on the fly
      if(!resultData[d1]) {
        // Create a value with a total counter to zero
        resultData[d1] = {TOTAL: 0};
      }
      // If the total counter does not know d2 set it to zero
      if(!resultData.TOTAL[d2]) {
        resultData.TOTAL[d2] = 0;
      }
      // If the contract type counter does not know d2 set it to zero
      if(!resultData[d1][d2]) {
        resultData[d1][d2] = 0;
      }
      // Count the value for the d2 and the different totals
      resultData[d1][d2] +=1;
      resultData[d1].TOTAL += 1;
      resultData.TOTAL[d2] += 1;
      resultData.TOTAL.TOTAL += 1;
    }
    return resultData;
  } 