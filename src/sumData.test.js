const sumData = require('./sumData');

test('Should Att to zero', async () => {
  const professions = sumData([]);
  // Check that the result is an object
  // The total values should be 0
});

test('Should get a list', async () => {
  const professions = sumData([{a: 1, b: 2}], (line) => ([line.a, line.b]));
  // Check that the result is an object
  // The total should be 1
  // One line and One column with total value 1
  // A colums with a key
  // A columns with a b key
})