module.exports = async function getMapProfessions (stream) {
    const reduced = new Map();
    for await (const line of stream) {
      reduced.set(line.id, line.category_name)
    }
    return reduced;
  } 